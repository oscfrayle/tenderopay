from rest_framework import serializers


class BuyerSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True
    )
    dni = serializers.CharField(
        required=True
    )
    balance = serializers.CharField(
        required=True
    )


class ShopkeeperSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True
    )
    dni = serializers.CharField(
        required=True
    )
    store = serializers.CharField(
        required=True
    )
    store_code = serializers.CharField(
        required=True
    )
    balance = serializers.IntegerField(
        required=True
    )


class ShoppingSerializer(serializers.Serializer):
    store = serializers.CharField(
        required=True
    )
    buyer = serializers.CharField(
        required=True
    )
    total_purchase = serializers.CharField(
        required=True
    )
