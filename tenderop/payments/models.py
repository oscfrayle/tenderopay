from django.db import models


class Shopkeeper(models.Model):
    name = models.CharField(
        max_length=30
    )
    dni = models.CharField(
        max_length=30
    )
    store = models.CharField(
        max_length=30
    )
    store_code = models.CharField(
        max_length=30
    )
    balance = models.IntegerField(
        default=0
    )


class Buyer(models.Model):
    name = models.CharField(
        max_length=30
    )
    dni = models.CharField(
        max_length=30
    )
    credit = models.IntegerField(
        default=180000
    )
    balance = models.IntegerField(
        default=0
    )


class Shopping(models.Model):
    store = models.CharField(
        max_length=10
    )
    buyer = models.CharField(
        max_length=10
    )
    total_purchase = models.CharField(
        max_length=10
    )
    date = models.DateTimeField(
        auto_now=True
    )
