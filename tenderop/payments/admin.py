from django.contrib import admin

from payments.models import Shopkeeper, Buyer, Shopping


@admin.register(Shopkeeper)
class ShopkeeperAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "dni",
        "store",
        "store_code",
        "balance"
    )

@admin.register(Buyer)
class BuyerAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "credit",
        "balance"
    )


@admin.register(Shopping)
class ShoppingAdmin(admin.ModelAdmin):
    list_display = (
        "store",
        "buyer",
        "total_purchase",
        "date"
    )
