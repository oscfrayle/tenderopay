# Generated by Django 4.0.1 on 2022-01-09 16:11

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Buyer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('dni', models.CharField(max_length=30)),
                ('balance', models.IntegerField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Shopkeeper',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('dni', models.CharField(max_length=30)),
                ('store', models.CharField(max_length=30)),
                ('store_code', models.CharField(max_length=30)),
                ('balance', models.IntegerField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Shopping',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('store', models.CharField(max_length=10)),
                ('buyer', models.CharField(max_length=10)),
                ('total_purchase', models.CharField(max_length=10)),
                ('date', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
