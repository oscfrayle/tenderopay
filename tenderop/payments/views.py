from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views import View


class HomeView(View):
    def get(self, request, **kwargs):
        context = {}
        return HttpResponse(
            render(
                request,
                "home.html",
                context,
            )
        )


class LoginView(View):
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        context = {}
        if self.request.user.is_authenticated:
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
        return HttpResponse(
            render(
                request,
                self.template_name,
                context)
        )

    class LogoutView(View):
        def get(self, request):
            logout(self.request)
            return HttpResponseRedirect(settings.LOGOUT_REDIRECT_URL)

    def post(self, request, *args, **kwargs):
        context = {}
        if request.POST:
            user = authenticate(
                username=User.objects.filter(
                    email=request.POST['email'].strip()
                ).first(),
                password=request.POST['password'].strip()
            )
        else:
            user = None
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
            else:
                context['error'] = "Tu cuenta esta desactivada, contácta al administrador!"
        else:
            context['error'] = "El usuario y/o la clave son incorrectos."
        return HttpResponse(render(request, self.template_name, context))


class BuyerView(View):
    def get(self, request, **kwargs):
        context = {}
        return HttpResponse(
            render(
                request,
                "hola",
                context,
            )
        )


class ShoppingView(View):
    def get(self, request, **kwargs):
        context = {}
        return HttpResponse(
            render(
                request,
                "config.html",
                context,
            )
        )


class BalanceView(View):
    template_name = "balance.html"

    def get(self, request, **kwargs):
        context = {}
        return HttpResponse(
            render(
                request,
                self.template_name,
                context,
            )
        )
