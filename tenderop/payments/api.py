import requests
from django.db.models import Sum
from django.http import JsonResponse
from django.views import View

from payments.models import Buyer, Shopping


class BalanceApiView(View):
    def get(self, request, **kwargs):

        buyer = Buyer.objects.get(
            dni=request.GET.get("buyer")
        )
        balance = Shopping.objects.filter(
            buyer=request.GET.get("buyer")
        ).aggregate(
            Sum('total_purchase')
        )
        if request.GET.get("pay") != "0":
            shopping = Shopping()
            shopping.buyer = request.GET.get("buyer")
            shopping.store = request.GET.get("store")
            shopping.total_purchase = request.GET.get("pay")
            shopping.save()
            requests.get(f"https://yo5wnx9ica.execute-api.us-east-1.amazonaws.com/api/sms/573133134054/(TenderoPay)_${request.GET.get('pay')},__verifica_tu_estado_de_cuenta!")

            context = {
                "balance": f"${int(buyer.credit)-int(balance['total_purchase__sum'])}",
                "credit": f"${buyer.credit}",
                "debt": f"${balance['total_purchase__sum']}",
                "buyer": request.GET.get("buyer"),
                "store": request.GET.get("store")
            }
        else:
            total = balance['total_purchase__sum']
            balance = buyer.credit
            if not total:
                total = 0
            if not balance:
                balance = 0
            context = {
                "balance": f"${int(balance)-int(total)}",
                "credit": f"${buyer.credit}",
                "debt": f"${total}",
                "buyer": request.GET.get("buyer"),
                "store": request.GET.get("store")
            }
            print(context)
        return JsonResponse(context)


class GetCreditApiView(View):
    def get(self, request, **kwargs):

        Buyer.objects.filter(
            dni=request.GET.get("buyer")
        ).update(
            credit="180000"
        )
        buyer = Buyer.objects.get(
            dni=request.GET.get("buyer")
        )
        print(buyer)
        context = {
                "credit": f"${str(buyer.credit)}",
                "buyer": request.GET.get("buyer")
            }
        return JsonResponse(context)
