from django.contrib import admin
from django.urls import path

from payments.api import BalanceApiView, GetCreditApiView
from payments.views import ShoppingView, HomeView, LoginView, BalanceView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('history/', ShoppingView.as_view()),
    path('login/', LoginView.as_view()),
    path('balance/', BalanceView.as_view()),
    path('api/v1/balance/', BalanceApiView.as_view()),
    path('api/v1/credit/', GetCreditApiView.as_view()),
    path('', HomeView.as_view()),
]

admin.site.site_header = 'TenderoPay Admin'