package com.example.t2.ui.pagar

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.findFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.t2.R
import com.example.t2.databinding.FragmentDashboardBinding
import com.example.t2.databinding.FragmentPagarBinding
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.zxing.integration.android.IntentIntegrator
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.json.JSONObject

class PagarFragment : Fragment() {

    private lateinit var notificationsViewModel: PagarViewModel
    private var _binding: FragmentPagarBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val tal = this


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProvider(this).get(PagarViewModel::class.java)

        _binding = FragmentPagarBinding.inflate(inflater, container, false)
        val root: View = binding.root

        println(binding.inputPay.text.toString())
        getBalance("0")

        binding.btnPagar.setOnClickListener {

            IntentIntegrator.forSupportFragment(this)
                .setPrompt("Escanea el QR del Tendero")
                .setBeepEnabled(true)
                .initiateScan()


        }

        return root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    fun getBalance(pay: String){
        val httpAsync = "https://f83062a6d1e4.ngrok.io/api/v1/balance?pay=${pay}&buyer=8012312&store=23456890"
            .httpGet()
            .responseString { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        println(ex)
                    }
                    is Result.Success -> {
                        val data = JSONObject(result.get())
                        println(data["balance"])
                        println(data["credit"])
                        println("llamando el api......")
                        binding.deudaActualTxt.text = " ${data["debt"] as CharSequence?} / ${data["credit"] as CharSequence?}"
                        binding.saldoActualTxt.text = " ${data["balance"] as CharSequence?}"

                    }
                }
            }

        httpAsync.join()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null) {
            if (result.contents != null) {
                getBalance(binding.inputPay.text.toString())
                Toast.makeText(
                    activity,
                    "Pago realizado, Gracias por su compra!",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(activity, "Cancelado", Toast.LENGTH_LONG).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

//    private fun initScanner(){
//        IntentIntegrator(this).initiateScan()
//    }

    }
}