package com.example.t2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.t2.databinding.ActivityMainBinding
import com.example.t2.ui.pagar.PagarFragment
import com.google.zxing.integration.android.IntentIntegrator

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.Theme_T2)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val navView: BottomNavigationView = binding.navView
        val logo = findViewById<ImageView>(R.id.imageView3)
        val pay = findViewById<Button>(R.id.pay)
        val sc = findViewById<Button>(R.id.sc_button)
        val collect = findViewById<Button>(R.id.collect)
        val saldo = findViewById<TextView>(R.id.saldo)
        val deudaTxt = findViewById<TextView>(R.id.deudaTxt)

        logo.setImageResource(R.drawable.logo)



        val httpAsync = "http://e0c0a3ef51b4.ngrok.io/api/v1/balance?pay=0&buyer=8012312&store=23456890"
            .httpGet()
            .responseString { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        println(ex)
                    }
                    is Result.Success -> {
                        val data = JSONObject(result.get())
                        println(data)
                        println(data["credit"])
                        saldo.text = data["balance"] as CharSequence?
                        deudaTxt.text = "Debes ${data["debt"]}"

                    }
                }
            }

        httpAsync.join()



        pay.setOnClickListener{
//            IntentIntegrator(this)
//                .setPrompt("Escanea el QR del Tendero")
//                .setBeepEnabled(true)
//                .initiateScan()

        }

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_dashboard, R.id.navigation_pagar, R.id.navigation_historial
            )
        )



        sc.setOnClickListener{
            val creditRequestFragment = CreditRequestFragment()
            creditRequestFragment.show(supportFragmentManager, "CRModalFragment")
                    val httpAsync = "http://e0c0a3ef51b4.ngrok.io/api/v1/credit?buyer=8012312"
            .httpGet()
            .responseString { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        println(ex)
                    }
                    is Result.Success -> {
                        val data = JSONObject(result.get())
                        println(data)
                        saldo.text = data["credit"] as CharSequence?

                    }
                }
            }


        httpAsync.join()
//            Thread().run { Thread.sleep(3000); saldo.text = "$50.000"}
        }

        collect.setOnClickListener{
            val collectFragment = CollectFragment()
            collectFragment.show(supportFragmentManager, "CollectFragment")

        }


        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)



    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data )
        val saldo = findViewById<TextView>(R.id.saldo)

        if (result != null){
            if(result.contents != null){

                Toast.makeText(this, "Pago realizado, Gracias por su compra!", Toast.LENGTH_LONG).show()
                saldo.text = "$120.000"
            }else{
                Toast.makeText(this, "Cancelado", Toast.LENGTH_LONG).show()
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data)
        }



    }



}