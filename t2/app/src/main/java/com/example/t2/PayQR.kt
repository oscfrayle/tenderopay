package com.example.t2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.t2.databinding.FragmentPagarBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.zxing.integration.android.IntentIntegrator

class PayQR : BottomSheetDialogFragment(){
    private var _binding: FragmentPagarBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        saveInstanceState: Bundle?
    ): View?{
        _binding = FragmentPagarBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.btnPagar.setOnClickListener {
            IntentIntegrator.forSupportFragment(this)
                .setPrompt("Escanea el QR del Tendero")
                .setBeepEnabled(true)
                .initiateScan()
            println("se pago")
        }
        return inflater.inflate(R.layout.fragment_pagar, container, false)
    }
}