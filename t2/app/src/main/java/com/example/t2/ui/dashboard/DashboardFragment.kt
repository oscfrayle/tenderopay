package com.example.t2.ui.dashboard

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.t2.R
import com.example.t2.databinding.FragmentDashboardBinding
import com.example.t2.ui.pagar.PagarViewModel
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import org.json.JSONObject

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textSaldo: TextView = binding.saldo
        val blockSw: Switch = binding.block
        val payBtn: Button = binding.pay

        val httpAsync = "http://e0c0a3ef51b4.ngrok.io/api/v1/balance?pay=0&buyer=8012312&store=23456890"
            .httpGet()
            .responseString { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        println(ex)
                    }
                    is Result.Success -> {
                        val data = JSONObject(result.get())
                        println(data)
                        println(data["credit"])
                        binding.saldo.text = data["credit"] as CharSequence?
                        binding.deudaTxt.text = "Debes ${data["debt"]}"

                    }
                }
            }

        httpAsync.join()

        if(blockSw.stateDescription == "ON"){
            blockSw.text = "Activo"
            Toast.makeText(activity, "Pago realizado, Gracias por su compra!", Toast.LENGTH_LONG).show()


        }else{
            blockSw.text = "Bloqueado"

        }

        blockSw.setOnClickListener {
            println(blockSw.stateDescription)
            if(blockSw.stateDescription == "ON"){
                blockSw.text = "Activo"


            }else{
                blockSw.text = "Bloqueado"

            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}